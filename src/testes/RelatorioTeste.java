package testes;

import controles.ServicoControle;
import dao.ServicoDAO;
import entidades.Servico;

public class RelatorioTeste {

	public static void main(String[] args) {
		
		try {
			ServicoDAO dao = new ServicoDAO();
			Servico servico = dao.buscarPorId(2);
			ServicoControle.gerarRelatorio(servico);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}
