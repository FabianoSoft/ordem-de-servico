package testes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DataTeste {

	public static void main(String[] args) {
		
		Date data = new Date();
		System.out.println(data);
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String dataString = formato.format(data); 
		System.out.println(dataString);
		
		try {
			Date data2 = formato.parse(dataString);
			System.out.println(data2);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		
		
	}
	
}
