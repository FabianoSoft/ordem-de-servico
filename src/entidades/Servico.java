package entidades;

import java.util.Date;

public class Servico {

	private int idServico;
	private String descricao;
	private Date dataInicio, dataFim;
	private String status;
	private float valor;
	private String obs;
	private Contato tecnico, cliente;

	public int getIdServico() {
		return idServico;
	}

	public void setIdServico(int idServico) {
		this.idServico = idServico;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public float getValor() {
		return valor;
	}

	public void setValor(float valor) {
		this.valor = valor;
	}

	public String getObs() {
		return obs;
	}

	public void setObs(String obs) {
		this.obs = obs;
	}

	public Contato getTecnico() {
		return tecnico;
	}

	public void setTecnico(Contato tecnico) {
		this.tecnico = tecnico;
	}

	public Contato getCliente() {
		return cliente;
	}

	public void setCliente(Contato cliente) {
		this.cliente = cliente;
	}

}