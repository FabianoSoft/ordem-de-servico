package conexoes;

import java.sql.Connection;
import java.sql.DriverManager;

import javax.swing.JOptionPane;

public class ConexaoMySQL {
	
	public Connection conexao() {
		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://127.0.0.1/ordem_de_servico", "root", "1root2");
			System.out.println("Conectado com sucesso");
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Erro ao conectar com banco de dados MySQL:" + e);
		}		
		return con;
	}
	
}
