package controles;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import conexoes.ConexaoMySQL;
import dao.ContatoDAO;
import entidades.Contato;

public class ContatoControle {
	
	public static void salvar(Contato contato) {
		try {
			ContatoDAO dao = new ContatoDAO();
			if (contato.getIdContato() > 0) {
				dao.update(contato);
			} else {
				dao.inserir(contato);
			}
			JOptionPane.showMessageDialog(null, "Contato " + contato.getNome() + " salvo com sucesso");
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Erro ao tentar salvar contato: " + e);
		}
	}
	
	public static void deletar(Contato contato) {
		try {
			ContatoDAO dao = new ContatoDAO();
			dao.delete(contato);
			JOptionPane.showMessageDialog(null, "Contato id: " + contato.getIdContato() + " deletado com sucesso");
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Erro ao tentar deletar contato: " + e);
		}
	}
	
	public static DefaultTableModel model(String pesquisar) {
		DefaultTableModel model = new DefaultTableModel(null, new Object[] {"id", "Nome", "Fone", "E-mail", "Endereço"}) {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		try {
			ContatoDAO dao = new ContatoDAO();
			List<Contato> lista = dao.listarTodos(pesquisar);
			for (Contato contato : lista) {
				model.addRow(new Object[] {
						contato.getIdContato(),
						contato.getNome(),
						contato.getFone(),
						contato.getEmail(),
						contato.getEndereco()
				});
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Erro ao tentar criar modelo de tabela: " + e);
		}
		return model;
	}

	public static List<Contato> listaContatos () {
		List<Contato> lista = new ArrayList<Contato>();
		try {
			ContatoDAO dao = new ContatoDAO();
			lista = dao.listarTodos("");
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Erro ao tentar listar todos os contatos: " + e);
		}
		return lista;
	}
	
	public static Contato stringContato(String srt) {
		String[] v = srt.split(":");
		int id = Integer.valueOf(v[0]);
		Contato contato = new Contato();
		try {
			ContatoDAO dao = new ContatoDAO();
			contato = dao.buscarPorId(id);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Erro ao tentar converter string para contato: " + e);
		}
		return contato;
	}
	
	public static String contatoString(Contato contato) {
		return contato.getIdContato() + ": " + contato.getNome();
	}
	
}
