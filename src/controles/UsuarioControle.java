package controles;

import java.sql.SQLException;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import dao.UsuarioDAO;
import entidades.Usuario;

public class UsuarioControle {
	
	public static Usuario usuario;
	
	public static boolean login(Usuario usuario) {
		UsuarioDAO dao = new UsuarioDAO();
		boolean existe = false;
		try {
			existe = dao.login(usuario);
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao tentar login: " + e);
		}
		return existe;
	}
	
	public static DefaultTableModel model(String pesquisar) {
		DefaultTableModel model = new DefaultTableModel(null, new Object[] {"id", "Nome", "Tipo"}) {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		try {
			UsuarioDAO dao = new UsuarioDAO();
			List<Usuario> lista = dao.listarTodos(pesquisar);
			for (Usuario usuario : lista) {
				model.addRow(new Object[] {
						usuario.getIdUsuario(),
						usuario.getNome(),
						usuario.getTipo(),
				});
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Erro ao tentar criar modelo de tabela: " + e);
		}
		return model;
	}
	
	public static Usuario buscarPorId(int id) {
		UsuarioDAO dao = new UsuarioDAO();
		Usuario usuario = new Usuario();
		try {
			usuario = dao.buscarPorId(id);
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao tentar buscar usuário por id: " + e);
		}
		return usuario;
	}
	
	public static void salvar(Usuario usuario) {
		try {
			UsuarioDAO dao = new UsuarioDAO();
			if (usuario.getIdUsuario() > 0) {
				dao.update(usuario);
			} else {
				dao.inserir(usuario);
			}
			JOptionPane.showMessageDialog(null, "Usuário " + usuario.getNome() + " salvo com sucesso");
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Erro ao tentar salvar usuário: " + e);
		}
	}
	
	public static void deletar(Usuario usuario) {
		try {
			UsuarioDAO dao = new UsuarioDAO();
			dao.delete(usuario);
			JOptionPane.showMessageDialog(null, "Usário id: " + usuario.getIdUsuario() + " deletado com sucesso");
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Erro ao tentar deletar usuário: " + e);
		}
	}
	
}
