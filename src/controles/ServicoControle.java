package controles;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import dao.ServicoDAO;
import entidades.Servico;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

public class ServicoControle {

	public static void salvar(Servico servico, JDialog jDialog) {
		try {
			ServicoDAO dao = new ServicoDAO();
			if (servico.getIdServico() > 0) {
				dao.update(servico);
			} else {
				servico.setIdServico(dao.inserir(servico));
			}
			if (JOptionPane.showConfirmDialog(null, "Ordem de Serviço salvo com sucesso\n\nDeseja imprimir esta Ordem de Serviço?", "System", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				jDialog.dispose();
				gerarRelatorio(servico);
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Erro ao tentar salvar ordem de serviço: " + e);
		}
	}
	
	public static void deletar(Servico servico) {
		try {
			ServicoDAO dao = new ServicoDAO();
			dao.delete(servico);
			JOptionPane.showMessageDialog(null, "Ordem de Serviço id: " + servico.getIdServico() + " deletado com sucesso");
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Erro ao tentar deletar ordem de serviço: " + e);
		}
	}
	
	public static DefaultTableModel model(String pesquisar) {
		DefaultTableModel model = new DefaultTableModel(null, new Object[] {"id", "Descrição", "Data de Início", "Data de Fim", "Status", "Valor", "Obs.", "Técnico", "Cliente"}) {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		try {
			ServicoDAO dao = new ServicoDAO();
			List<Servico> lista = dao.listarTodos(pesquisar);
			for (Servico servico : lista) {
				model.addRow(new Object[] {
						servico.getIdServico(),
						servico.getDescricao(),
						/*servico.getDataInicio() == null ? null : ServicoControle.dateString(servico.getDataInicio()),
						servico.getDataFim() == null ? null : ServicoControle.dateString(servico.getDataFim()),*/
						servico.getDataInicio(),
						servico.getDataFim(),
						servico.getStatus(),
						String.valueOf(servico.getValor()).replace(".", ","),
						servico.getObs(),
						ContatoControle.contatoString(servico.getTecnico()),
						ContatoControle.contatoString(servico.getCliente()),
				});
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Erro ao tentar criar modelo de tabela: " + e);
		}
		return model;
	}
	
	public static Date stringDate(String data) {
		SimpleDateFormat exibicao = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = null;
		try {
			date = exibicao.parse(data);
		} catch (ParseException e) {
			//e.printStackTrace(); sem data
		}
		return date;
	}
	
	public static String dateString(Date data) {
		SimpleDateFormat exibicao = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		return exibicao.format(data);
	}
	
	public static Servico buscarPorId(int id) {
		ServicoDAO dao = new ServicoDAO();
		Servico servico = new Servico();
		try {
			servico = dao.buscarPorId(id);
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao tentar buscar serviço por id: " + e);
		}
		return servico;
	}
	
	public static void gerarRelatorio(Servico servico) {
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("idServico", servico.getIdServico());
			map.put("descricao", servico.getDescricao());
			map.put("dataInicio", servico.getDataInicio());
			map.put("dataFim", servico.getDataFim());
			map.put("status", servico.getStatus());
			map.put("valor", ("R$ " + servico.getValor()).replace(".", ","));
			map.put("obs", servico.getObs());
			map.put("nomeTecnico", servico.getTecnico().getNome());
			map.put("foneTecnico", servico.getTecnico().getFone());
			map.put("emailTecnico", servico.getTecnico().getEmail());
			map.put("idCliente", servico.getCliente().getIdContato());
			map.put("nomeCliente", servico.getCliente().getNome());
			map.put("foneCliente", servico.getCliente().getFone());
			map.put("emailCliente", servico.getCliente().getEmail());
			map.put("enderecoCliente", servico.getCliente().getEndereco());
			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			list.add(map);
			JRDataSource jrDataSource = new JRBeanCollectionDataSource(list);
			JasperReport jasperReport = JasperCompileManager.compileReport(ServicoControle.class.getResourceAsStream("/relatorios/OrdemDeServico.jrxml"));
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, null, jrDataSource);
			// JasperPrint jasperPrint = JasperFillManager.fillReport(ServicoControle.class.getResourceAsStream("/relatorios/OrdemDeServico.jasper"), null, jrDataSource);
			JasperViewer.viewReport(jasperPrint, false);
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Erro ao tentar gerar relatório: " + e.getMessage());
		}
	}
	
}
