package dao;

import java.awt.RenderingHints.Key;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Statement;

import conexoes.ConexaoMySQL;
import controles.ServicoControle;
import entidades.Contato;
import entidades.Servico;

public class ServicoDAO {

	public int inserir(Servico servico) throws SQLException {
		ConexaoMySQL conexaoMySQL = new ConexaoMySQL();
		Connection con = conexaoMySQL.conexao();
		String insert = "INSERT INTO servico (descricao, data_inicio, data_fim, status, valor, obs, id_tecnico, id_cliente) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement ps = con.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);
		int i = 0;
		ps.setString(++i, servico.getDescricao());
		ps.setObject(++i, servico.getDataInicio());
		ps.setObject(++i, servico.getDataFim());
		ps.setString(++i, servico.getStatus());
		ps.setFloat(++i, servico.getValor());
		ps.setString(++i, servico.getObs());
		ps.setInt(++i, servico.getTecnico().getIdContato());
		ps.setInt(++i, servico.getCliente().getIdContato());
		ps.execute();
		ResultSet rs = ps.getGeneratedKeys();
		rs.first();
		int chavePrimaria = rs.getInt(1);
		ps.close();
		con.close();
		return chavePrimaria;
	}
	
	public void update(Servico servico) throws SQLException {
		ConexaoMySQL conexaoMySQL = new ConexaoMySQL();
		Connection con = conexaoMySQL.conexao();
		String update = "UPDATE servico SET descricao = ?, data_inicio = ?, data_fim = ?, status = ?, valor = ?, obs = ?, id_tecnico = ?, id_cliente = ? WHERE id_servico = ?";
		PreparedStatement ps = con.prepareStatement(update);
		int i = 0;
		ps.setString(++i, servico.getDescricao());
		ps.setObject(++i, servico.getDataInicio());
		ps.setObject(++i, servico.getDataFim());
		ps.setString(++i, servico.getStatus());
		ps.setFloat(++i, servico.getValor());
		ps.setString(++i, servico.getObs());
		ps.setInt(++i, servico.getTecnico().getIdContato());
		ps.setInt(++i, servico.getCliente().getIdContato());
		ps.setInt(++i, servico.getIdServico());
		ps.execute();
		ps.close();
		con.close();
	}
	
	public void delete(Servico servico) throws SQLException {
		ConexaoMySQL conexaoMySQL = new ConexaoMySQL();
		Connection con = conexaoMySQL.conexao();
		String delete = "DELETE FROM servico WHERE id_servico = ?";
		PreparedStatement ps = con.prepareStatement(delete);
		int i = 0;
		ps.setInt(++i, servico.getIdServico());
		ps.execute();
		ps.close();
		con.close();
	}
	
	public List<Servico> listarTodos(String pesquisar) throws SQLException {
		ConexaoMySQL conexaoMySQL = new ConexaoMySQL();
		Connection con = conexaoMySQL.conexao();
		String select = "SELECT * FROM servico WHERE id_servico LIKE ? OR descricao LIKE ? OR data_inicio LIKE ? OR data_fim LIKE ? OR status LIKE ?  OR valor LIKE ? OR obs LIKE ? OR id_tecnico IN (SELECT id_contato FROM contato WHERE nome LIKE ?) OR id_cliente IN (SELECT id_contato FROM contato WHERE nome LIKE ?) ORDER BY id_servico DESC";
		PreparedStatement ps = con.prepareStatement(select);
		for (int i = 1; i <= 9; i++) {
			ps.setObject(i, "%" + pesquisar + "%");
		}
		ResultSet rs = ps.executeQuery();
		List<Servico> lista = new ArrayList<Servico>();
		ContatoDAO dao = new ContatoDAO();
		while (rs.next()) {
			Servico servico = new Servico();
			int i = 0;
			servico.setIdServico(rs.getInt(++i));
			servico.setDescricao(rs.getString(++i));
			servico.setDataInicio(rs.getTimestamp(++i));
			servico.setDataFim(rs.getTimestamp(++i));
			servico.setStatus(rs.getString(++i));
			servico.setValor(rs.getFloat(++i));
			servico.setObs(rs.getString(++i));
			servico.setTecnico(dao.buscarPorId(rs.getInt(++i)));
			servico.setCliente(dao.buscarPorId(rs.getInt(++i)));
			lista.add(servico);
		}
		rs.close();
		ps.close();
		con.close();
		return lista;
	}
	
	public Servico buscarPorId(int id) throws SQLException {
		ConexaoMySQL conexaoMySQL = new ConexaoMySQL();
		Connection con = conexaoMySQL.conexao();
		String select = "SELECT * FROM servico WHERE id_servico = ?";
		PreparedStatement ps = con.prepareStatement(select);
		ps.setObject(1, id);
		ResultSet rs = ps.executeQuery();
		rs.first();
		ContatoDAO dao = new ContatoDAO();
		Servico servico = new Servico();
		int i = 0;
		servico.setIdServico(rs.getInt(++i));
		servico.setDescricao(rs.getString(++i));
		servico.setDataInicio(rs.getTimestamp(++i));
		servico.setDataFim(rs.getTimestamp(++i));
		servico.setStatus(rs.getString(++i));
		servico.setValor(rs.getFloat(++i));
		servico.setObs(rs.getString(++i));
		servico.setTecnico(dao.buscarPorId(rs.getInt(++i)));
		servico.setCliente(dao.buscarPorId(rs.getInt(++i)));
		rs.close();
		ps.close();
		con.close();
		return servico;
	}
	
}
