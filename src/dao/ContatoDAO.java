package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import conexoes.ConexaoMySQL;
import entidades.Contato;

public class ContatoDAO {

	public void inserir(Contato contato) throws SQLException {
		ConexaoMySQL conexaoMySQL = new ConexaoMySQL();
		Connection con = conexaoMySQL.conexao();
		String insert = "INSERT INTO contato (nome, fone, email, endereco) VALUES (?, ?, ?, ?)";
		PreparedStatement ps = con.prepareStatement(insert);
		int i = 0;
		ps.setString(++i, contato.getNome());
		ps.setString(++i, contato.getFone());
		ps.setString(++i, contato.getEmail());
		ps.setString(++i, contato.getEndereco());
		ps.execute();
		ps.close();
		con.close();
	}
	
	public void update(Contato contato) throws SQLException {
		ConexaoMySQL conexaoMySQL = new ConexaoMySQL();
		Connection con = conexaoMySQL.conexao();
		String update = "UPDATE contato SET nome = ?, fone = ?, email = ?, endereco = ? WHERE id_contato = ?";
		PreparedStatement ps = con.prepareStatement(update);
		int i = 0;
		ps.setString(++i, contato.getNome());
		ps.setString(++i, contato.getFone());
		ps.setString(++i, contato.getEmail());
		ps.setString(++i, contato.getEndereco());
		ps.setInt(++i, contato.getIdContato());
		ps.execute();
		ps.close();
		con.close();
	}
	
	public void delete(Contato contato) throws SQLException {
		ConexaoMySQL conexaoMySQL = new ConexaoMySQL();
		Connection con = conexaoMySQL.conexao();
		String delete = "DELETE FROM contato WHERE id_contato = ?";
		PreparedStatement ps = con.prepareStatement(delete);
		int i = 0;
		ps.setInt(++i, contato.getIdContato());
		ps.execute();
		ps.close();
		con.close();
	}
	
	public List<Contato> listarTodos(String pesquisar) throws SQLException {
		ConexaoMySQL conexaoMySQL = new ConexaoMySQL();
		Connection con = conexaoMySQL.conexao();
		String select = "SELECT * FROM contato WHERE id_contato LIKE ? OR nome LIKE ? OR fone LIKE ? OR email LIKE ? OR endereco LIKE ? ORDER BY nome";
		PreparedStatement ps = con.prepareStatement(select);
		for (int i = 1; i <= 5; i++) {
			ps.setObject(i, "%" + pesquisar + "%");
		}
		ResultSet rs = ps.executeQuery();
		List<Contato> lista = new ArrayList<Contato>();
		while (rs.next()) {
			Contato contato = new Contato();
			int i = 0;
			contato.setIdContato(rs.getInt(++i));
			contato.setNome(rs.getString(++i));
			contato.setFone(rs.getString(++i));
			contato.setEmail(rs.getString(++i));
			contato.setEndereco(rs.getString(++i));
			lista.add(contato);
		}
		rs.close();
		ps.close();
		con.close();
		return lista;
	}
	
	public Contato buscarPorId(int id) throws SQLException {
		ConexaoMySQL conexaoMySQL = new ConexaoMySQL();
		Connection con = conexaoMySQL.conexao();
		String select = "SELECT * FROM contato WHERE id_contato = ?";
		PreparedStatement ps = con.prepareStatement(select);
		ps.setObject(1, id);
		ResultSet rs = ps.executeQuery();
		rs.first();
		Contato contato = new Contato();
		int i = 0;
		contato.setIdContato(rs.getInt(++i));
		contato.setNome(rs.getString(++i));
		contato.setFone(rs.getString(++i));
		contato.setEmail(rs.getString(++i));
		contato.setEndereco(rs.getString(++i));
		rs.close();
		ps.close();
		con.close();
		return contato;
	}
	
}
