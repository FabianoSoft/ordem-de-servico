package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.UpdatableResultSet;

import conexoes.ConexaoMySQL;
import controles.UsuarioControle;
import entidades.Servico;
import entidades.Usuario;

public class UsuarioDAO {

	public boolean login(Usuario usuario) throws SQLException {
		String sql = "SELECT * FROM usuario WHERE nome = ? AND senha = ?";
		ConexaoMySQL mySQL = new ConexaoMySQL();
		Connection con = mySQL.conexao();
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, usuario.getNome());
		ps.setString(2, usuario.getSenha());
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			UsuarioControle.usuario = buscarPorId(rs.getInt(1));
			return true;
		}
		rs.close();
		ps.close();
		con.close();
		return false;
	}
	
	public List<Usuario> listarTodos(String pesquisar) throws SQLException {
		ConexaoMySQL conexaoMySQL = new ConexaoMySQL();
		Connection con = conexaoMySQL.conexao();
		String select = "SELECT * FROM usuario WHERE id_usuario LIKE ? OR nome LIKE ? OR tipo LIKE ?;";
		PreparedStatement ps = con.prepareStatement(select);
		for (int i = 1; i <= 3; i++) {
			ps.setObject(i, "%" + pesquisar + "%");
		}
		ResultSet rs = ps.executeQuery();
		List<Usuario> lista = new ArrayList<Usuario>();
		while (rs.next()) {
			Usuario usuario = new Usuario();
			int i = 0;
			usuario.setIdUsuario(rs.getInt(++i));
			usuario.setNome(rs.getString(++i));
			usuario.setSenha(rs.getString(++i));
			usuario.setTipo(rs.getString(++i));
			lista.add(usuario);
		}
		rs.close();
		ps.close();
		con.close();
		return lista;
	}
	
	public Usuario buscarPorId(int id) throws SQLException {
		ConexaoMySQL conexaoMySQL = new ConexaoMySQL();
		Connection con = conexaoMySQL.conexao();
		String select = "SELECT * FROM usuario WHERE id_usuario = ?";
		PreparedStatement ps = con.prepareStatement(select);
		ps.setObject(1, id);
		ResultSet rs = ps.executeQuery();
		rs.first();
		ContatoDAO dao = new ContatoDAO();
		Usuario usuario = new Usuario();
		int i = 0;
		usuario.setIdUsuario(rs.getInt(++i));
		usuario.setNome(rs.getString(++i));
		usuario.setSenha(rs.getString(++i));
		usuario.setTipo(rs.getString(++i));
		rs.close();
		ps.close();
		con.close();
		return usuario;
	}
	
	public void inserir(Usuario usuario) throws SQLException {
		ConexaoMySQL conexaoMySQL = new ConexaoMySQL();
		Connection con = conexaoMySQL.conexao();
		String insert = "INSERT INTO usuario (nome, senha, tipo) VALUES (?, ?, ?)";
		PreparedStatement ps = con.prepareStatement(insert);
		int i = 0;
		ps.setString(++i, usuario.getNome());
		ps.setString(++i, usuario.getSenha());
		ps.setString(++i, usuario.getTipo());
		ps.execute();
		ps.close();
		con.close();
	}
	
	public void update(Usuario usuario) throws SQLException {
		ConexaoMySQL conexaoMySQL = new ConexaoMySQL();
		Connection con = conexaoMySQL.conexao();
		String update = "UPDATE usuario SET nome = ?, senha = ?, tipo = ? WHERE id_usuario = ?";
		PreparedStatement ps = con.prepareStatement(update);
		int i = 0;
		ps.setString(++i, usuario.getNome());
		ps.setString(++i, usuario.getSenha());
		ps.setString(++i, usuario.getTipo());
		ps.setInt(++i, usuario.getIdUsuario());
		ps.execute();
		ps.close();
		con.close();
	}
	
	public void delete(Usuario usuario) throws SQLException {
		ConexaoMySQL conexaoMySQL = new ConexaoMySQL();
		Connection con = conexaoMySQL.conexao();
		String delete = "DELETE FROM usuario WHERE id_usuario = ?";
		PreparedStatement ps = con.prepareStatement(delete);
		int i = 0;
		ps.setInt(++i, usuario.getIdUsuario());
		ps.execute();
		ps.close();
		con.close();
	}
	
}
