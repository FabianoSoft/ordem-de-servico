package forms;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.UIManager;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.JTextField;
import javax.swing.RowSorter;
import javax.swing.JFormattedTextField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.text.MaskFormatter;

import controles.ContatoControle;
import entidades.Contato;

import javax.swing.JTable;
import javax.swing.DefaultRowSorter;
import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class FrmContato extends JDialog {

	private JPanel contentPane;
	private JTextField txtId;
	private JTextField txtNome;
	private JTextField txtEmail;
	private JTextField txtPesquisar;
	private JTable table;
	private JFormattedTextField frmtdtxtfldFone;
	private JTextArea txtrEndereco;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmContato frame = new FrmContato();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmContato() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				atualizar();
			}
			@Override
			public void windowClosed(WindowEvent e) {
				FrmMenu.initialize();
			}
		});
		setModal(true);
		setTitle("Contatos");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(834, 700);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblGerenciamentoDeContatos = new JLabel("Gerenciamento de Contatos");
		lblGerenciamentoDeContatos.setHorizontalAlignment(SwingConstants.CENTER);
		lblGerenciamentoDeContatos.setForeground(Color.BLUE);
		lblGerenciamentoDeContatos.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 25));
		lblGerenciamentoDeContatos.setBounds(12, 13, 785, 31);
		contentPane.add(lblGerenciamentoDeContatos);
		
		JPanel panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.setBounds(22, 64, 775, 592);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblId = new JLabel("id:");
		lblId.setHorizontalAlignment(SwingConstants.RIGHT);
		lblId.setBounds(78, 28, 56, 16);
		panel.add(lblId);
		
		txtId = new JTextField();
		txtId.setEditable(false);
		txtId.setText("id");
		txtId.setBounds(146, 22, 122, 28);
		panel.add(txtId);
		txtId.setColumns(10);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNome.setBounds(78, 68, 56, 16);
		panel.add(lblNome);
		
		txtNome = new JTextField();
		txtNome.setText("Nome");
		txtNome.setBounds(146, 62, 537, 28);
		panel.add(txtNome);
		txtNome.setColumns(10);
		
		JLabel lblFone = new JLabel("Fone:");
		lblFone.setHorizontalAlignment(SwingConstants.RIGHT);
		lblFone.setBounds(78, 108, 56, 16);
		panel.add(lblFone);
		
		try {
			MaskFormatter mascaraFone = new MaskFormatter("(##) # ####-####");
			frmtdtxtfldFone = new JFormattedTextField(mascaraFone);
		} catch (Exception e) {
			e.getMessage();
		}
		frmtdtxtfldFone.setText("Fone");
		frmtdtxtfldFone.setBounds(146, 102, 150, 28);
		panel.add(frmtdtxtfldFone);
		
		JLabel lblEmail = new JLabel("E-mail:");
		lblEmail.setHorizontalAlignment(SwingConstants.RIGHT);
		lblEmail.setBounds(308, 108, 56, 16);
		panel.add(lblEmail);
		
		txtEmail = new JTextField();
		txtEmail.setText("E-mail");
		txtEmail.setBounds(376, 102, 307, 28);
		panel.add(txtEmail);
		txtEmail.setColumns(10);
		
		JLabel lblEndereo = new JLabel("Endereço:");
		lblEndereo.setHorizontalAlignment(SwingConstants.RIGHT);
		lblEndereo.setBounds(78, 151, 56, 16);
		panel.add(lblEndereo);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(146, 142, 537, 75);
		panel.add(scrollPane);
		
		txtrEndereco = new JTextArea();
		txtrEndereco.setText("Endereco");
		scrollPane.setViewportView(txtrEndereco);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Tabela de técnicos", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(6, 229, 763, 292);
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblPesquisar = new JLabel("Pesquisar:");
		lblPesquisar.setFont(new Font("SansSerif", Font.ITALIC, 12));
		lblPesquisar.setBounds(33, 26, 58, 16);
		panel_1.add(lblPesquisar);
		
		txtPesquisar = new JTextField();
		txtPesquisar.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				atualizarTabela();
			}
			@Override
			public void keyPressed(KeyEvent e) {
				esc(e);
			}
		});
		txtPesquisar.setText("Pesquisar");
		txtPesquisar.setBounds(103, 20, 625, 28);
		panel_1.add(txtPesquisar);
		txtPesquisar.setColumns(10);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(16, 54, 730, 220);
		panel_1.add(scrollPane_1);
		
		table = new JTable();
		table.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				tabelaFormulario();
			}
			@Override
			public void keyPressed(KeyEvent e) {
				esc(e);
			}
		});
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				tabelaFormulario();
			}
		});
		scrollPane_1.setViewportView(table);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_2.setBounds(6, 533, 763, 53);
		panel.add(panel_2);
		panel_2.setLayout(new GridLayout(0, 6, 0, 0));
		
		JButton btnAtualizar = new JButton("Atualizar");
		btnAtualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				atualizar();
			}
		});
		btnAtualizar.setFont(new Font("Calibri", Font.BOLD, 15));
		btnAtualizar.setMnemonic('a');
		panel_2.add(btnAtualizar);
		
		JButton btnNovo = new JButton("Novo");
		btnNovo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				novo();
			}
		});
		btnNovo.setFont(new Font("Calibri", Font.BOLD, 15));
		panel_2.add(btnNovo);
		btnNovo.setMnemonic('n');
		
		JButton btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				salvar();
			}
		});
		btnSalvar.setFont(new Font("Calibri", Font.BOLD, 15));
		panel_2.add(btnSalvar);
		btnSalvar.setMnemonic('s');
		
		JButton btnLimpar = new JButton("Limpar");
		btnLimpar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				limpar();
			}
		});
		btnLimpar.setFont(new Font("Calibri", Font.BOLD, 15));
		panel_2.add(btnLimpar);
		btnLimpar.setMnemonic('l');
		
		JButton btnDeletar = new JButton("Deletar");
		btnDeletar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				deletar();
			}
		});
		btnDeletar.setFont(new Font("Calibri", Font.BOLD, 15));
		panel_2.add(btnDeletar);
		btnDeletar.setMnemonic('d');
		
		JButton btnFechar = new JButton("Fechar");
		btnFechar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				fechar();
			}
		});
		btnFechar.setFont(new Font("Calibri", Font.BOLD, 15));
		panel_2.add(btnFechar);
		btnFechar.setMnemonic('f');
	}
	
	private void fechar() {
		dispose();
	}
	
	private void limpar() {
		txtPesquisar.setText(null);
		txtId.setText(null);
		txtNome.setText(null);
		frmtdtxtfldFone.setText(null);
		txtEmail.setText(null);
		txtrEndereco.setText(null);
		txtPesquisar.requestFocus();
	}
	
	private void novo() {
		limpar();
		txtNome.requestFocus();
	}
	
	private void salvar() {
		if (txtNome.getText().isEmpty()){
			JOptionPane.showMessageDialog(null, "Digite o nome");
			txtNome.requestFocus();
			return;
		}
		Contato contato = new Contato();
		try {
			contato.setIdContato(Integer.valueOf(txtId.getText().trim()));
		} catch (Exception e) {
			contato.setIdContato(0);
		}
		contato.setNome(txtNome.getText().trim());
		contato.setFone(frmtdtxtfldFone.getText().trim());
		contato.setEmail(txtEmail.getText().trim());
		contato.setEndereco(txtrEndereco.getText().trim());
		ContatoControle.salvar(contato);
		atualizar();
	}
	
	private void atualizarTabela() {
		DefaultTableModel model = ContatoControle.model(txtPesquisar.getText().trim());
		table.setModel(model);
		RowSorter<TableModel> sorter = new TableRowSorter<TableModel>(model);
		table.setRowSorter(sorter);
		table.getColumnModel().getColumn(0).setPreferredWidth(7);
	}
	
	private void atualizar() {
		limpar();
		atualizarTabela();
	}
	
	private void tabelaFormulario() {
		int coluna = 0;
		int linha = table.getSelectedRow();
		if (linha >= 0) {
			txtId.setText(table.getValueAt(linha, coluna++).toString());
			txtNome.setText(table.getValueAt(linha, coluna++).toString());
			frmtdtxtfldFone.setText(table.getValueAt(linha, coluna++).toString());
			txtEmail.setText(table.getValueAt(linha, coluna++).toString());
			txtrEndereco.setText(table.getValueAt(linha, coluna++).toString());
		}
	}
	
	private void esc(KeyEvent e){
		if (e.getKeyCode() == 27) {// tecla esc
			atualizar();
		}
	}
	
	private void deletar() {
		if (txtId.getText().isEmpty()){
			JOptionPane.showMessageDialog(null, "Escolha um contato clicando na tabela");
			return;
		}
		if (JOptionPane.showConfirmDialog(null, "Tem certeza que deseja deletar este contato?", "Sistema", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			Contato contato = new Contato();
			try {
				contato.setIdContato(Integer.valueOf(txtId.getText().trim()));
			} catch (Exception e) {
				contato.setIdContato(0);
			}
			ContatoControle.deletar(contato);
			atualizar();
		}		
	}
	
}
