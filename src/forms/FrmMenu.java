package forms;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import controles.ContatoControle;
import controles.UsuarioControle;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JLayeredPane;
import java.awt.GridLayout;
import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;

public class FrmMenu extends JFrame {

	private boolean aberto = true;
	
	private JPanel contentPane;
	private static JMenuItem mntmServico;
	private JLabel labelImagem;
	private static JMenuItem mntmUsuario;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmMenu frame = new FrmMenu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmMenu() {
		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent arg0) {
				atualizarImagem();
			}
		});
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent arg0) {
				FrmLogin login = new FrmLogin();
				login.setVisible(true);
				aberto = false;
			}
			@Override
			public void windowOpened(WindowEvent arg0) {
				initialize();
				temp();
			}
		});
		setTitle("Ordem de Serviço");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(1000, 700);
		setLocationRelativeTo(null);
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnCadastro = new JMenu("Cadastro");
		mnCadastro.setMnemonic('c');
		menuBar.add(mnCadastro);
		
		JMenuItem mntmContato = new JMenuItem("Contato");
		mntmContato.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
		mntmContato.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FrmContato contato = new FrmContato();
				contato.setVisible(true);
			}
		});
		mnCadastro.add(mntmContato);
		
		mntmServico = new JMenuItem("Serviço");
		mntmServico.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0));
		mntmServico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				FrmServico servico = new FrmServico();
				servico.setVisible(true);
			}
		});
		mnCadastro.add(mntmServico);
		
		mntmUsuario = new JMenuItem("Usuário");
		mntmUsuario.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F3, 0));
		mntmUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				FrmUsuario usuario = new FrmUsuario();
				usuario.setVisible(true);
			}
		});
		mnCadastro.add(mntmUsuario);
		
		JMenu mnFerramentas = new JMenu("Ferramentas");
		mnFerramentas.setMnemonic('f');
		menuBar.add(mnFerramentas);
		
		JMenuItem mntmBlocoDeNotas = new JMenuItem("Bloco de Notas");
		mntmBlocoDeNotas.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, 0));
		mntmBlocoDeNotas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ferramentas("notepad");
			}
		});
		mnFerramentas.add(mntmBlocoDeNotas);
		
		JMenuItem mntmWordpad = new JMenuItem("WordPad");
		mntmWordpad.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
		mntmWordpad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ferramentas("write");
			}
		});
		mnFerramentas.add(mntmWordpad);
		
		JMenuItem mntmPaint = new JMenuItem("Paint");
		mntmPaint.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F6, 0));
		mntmPaint.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ferramentas("mspaint");
			}
		});
		mnFerramentas.add(mntmPaint);
		
		JMenuItem mntmWindowsExplorer = new JMenuItem("Windows Explorer");
		mntmWindowsExplorer.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F7, 0));
		mntmWindowsExplorer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ferramentas("explorer");
			}
		});
		mnFerramentas.add(mntmWindowsExplorer);
		
		JMenuItem mntmCalculadora = new JMenuItem("Calculadora");
		mntmCalculadora.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F8, 0));
		mntmCalculadora.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ferramentas("calc");
			}
		});
		mnFerramentas.add(mntmCalculadora);
		
		JMenu mnSair = new JMenu("Sair");
		mnSair.setMnemonic('s');
		menuBar.add(mnSair);
		
		JMenuItem mntmSair = new JMenuItem("Sair");
		mntmSair.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F9, 0));
		mntmSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fechar();
			}
		});
		mnSair.add(mntmSair);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(0, 1, 0, 0));
		
		labelImagem = new JLabel("");
		contentPane.add(labelImagem);
		labelImagem.setHorizontalAlignment(SwingConstants.CENTER);
	}
	
	private void fechar(){
		dispose();
	}
	
	public static void initialize() {
		try {
			if (ContatoControle.listaContatos().size() > 0) {
				mntmServico.setVisible(true);
			} else {
				mntmServico.setVisible(false);
			}
			if (UsuarioControle.usuario.getTipo().equals("Padrão")) {
				mntmUsuario.setVisible(false);
			}
		} catch (Exception e) {
			//JOptionPane.showMessageDialog(null, e); contato aberto sem menu
		}
	}
	
	private void ferramentas(String exe) {
		try {
			Runtime.getRuntime().exec(exe);
		} catch (IOException e) {
			// TODO Bloco catch gerado automaticamente
			e.printStackTrace();
		}
	}
	
	private void atualizarImagem() {
		try {
			ImageIcon ii = new ImageIcon(FrmMenu.class.getResource("/imagens/20141219205334_660_420.jpg"));
			Image i = ii.getImage();
			Image i2 = i.getScaledInstance(labelImagem.getWidth(), labelImagem.getHeight(), Image.SCALE_DEFAULT);
			ImageIcon ii2 = new ImageIcon(i2);
			labelImagem.setIcon(ii2);
		} catch (Exception e) {
			// TODO Bloco catch gerado automaticamente
			//e.printStackTrace();
		}
	}
	
	private void temp() {
		Thread t = new Thread() {
			@Override
			public void run() {
				try {
					String t = getTitle();
					while(aberto) {//System.out.println(aberto);
						setTitle(t + ": " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new GregorianCalendar().getTime()));
						Thread.sleep(1000);
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		};
		t.start();
	}
}
