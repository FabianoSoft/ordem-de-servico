package forms;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.UIManager;
import javax.swing.JTabbedPane;
import javax.swing.border.EtchedBorder;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.RowSorter;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import entidades.Servico;
import entidades.Usuario;

import javax.swing.JButton;
import javax.swing.JPasswordField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.KeyAdapter;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import controles.ContatoControle;
import controles.ServicoControle;
import controles.UsuarioControle;

import javax.swing.event.ChangeEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class FrmUsuario extends JDialog {

	private JPanel contentPane;
	private JTextField txtPesquisar;
	private JTable table;
	private JTextField txtNome;
	private JPasswordField pwdSenha;
	private JPasswordField pwdConfirmaSenha;
	private JLabel txtId;
	private JComboBox comboBox;
	private JTabbedPane tabbedPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmUsuario frame = new FrmUsuario();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmUsuario() {
		setModal(true);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent arg0) {
				atualizar();
			}
		});
		setResizable(false);
		setTitle("Usuário");
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setSize(688, 417);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(0, 1, 0, 0));
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		tabbedPane.addTab("Usu\u00E1rios", null, panel, null);
		panel.setLayout(null);
		
		JLabel lblPesquisar = new JLabel("Pesquisar:");
		lblPesquisar.setFont(new Font("SansSerif", Font.ITALIC, 12));
		lblPesquisar.setBounds(6, 12, 58, 16);
		panel.add(lblPesquisar);
		
		txtPesquisar = new JTextField();
		txtPesquisar.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				esc(arg0);
			}
			@Override
			public void keyReleased(KeyEvent e) {
				atualizarTabela();
			}
		});
		txtPesquisar.setText("Pesquisar");
		txtPesquisar.setColumns(10);
		txtPesquisar.setBounds(76, 6, 586, 28);
		panel.add(txtPesquisar);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(6, 46, 656, 293);
		panel.add(scrollPane);
		
		table = new JTable();
		table.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == 10) {//10 � a tecla enter
					editar();
				}
			}
		});
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (arg0.getClickCount() > 1) {					
					editar();
				}
			}
		});
		scrollPane.setViewportView(table);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		tabbedPane.addTab("Novo", null, panel_1, null);
		panel_1.setLayout(null);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Controles", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_2.setBounds(6, 243, 656, 96);
		panel_1.add(panel_2);
		panel_2.setLayout(new GridLayout(0, 3, 0, 0));
		
		JButton cmdSalvar = new JButton("Salvar");
		cmdSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				salvar();
			}
		});
		cmdSalvar.setMnemonic('s');
		cmdSalvar.setFont(new Font("Calibri", Font.BOLD, 18));
		panel_2.add(cmdSalvar);
		
		JButton cmdCancelar = new JButton("Cancelar");
		cmdCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cancelar();
			}
		});
		cmdCancelar.setMnemonic('c');
		cmdCancelar.setFont(new Font("Calibri", Font.BOLD, 18));
		panel_2.add(cmdCancelar);
		
		JButton cmdDeletar = new JButton("Deletar");
		cmdDeletar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				deletar();
			}
		});
		cmdDeletar.setMnemonic('d');
		cmdDeletar.setFont(new Font("Calibri", Font.BOLD, 18));
		panel_2.add(cmdDeletar);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new TitledBorder(null, "Dados", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_3.setBounds(6, 6, 656, 225);
		panel_1.add(panel_3);
		panel_3.setLayout(null);
		
		JLabel lblId = new JLabel("id:");
		lblId.setBounds(181, 20, 13, 16);
		panel_3.add(lblId);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setBounds(157, 54, 37, 16);
		panel_3.add(lblNome);
		
		JLabel lblSenha = new JLabel("Senha:");
		lblSenha.setBounds(155, 94, 39, 16);
		panel_3.add(lblSenha);
		
		JLabel lblConfirmaSenha = new JLabel("Confirma senha:");
		lblConfirmaSenha.setBounds(102, 136, 92, 16);
		panel_3.add(lblConfirmaSenha);
		
		JLabel lblTipo = new JLabel("Tipo:");
		lblTipo.setBounds(167, 175, 27, 16);
		panel_3.add(lblTipo);
		
		txtId = new JLabel("id");
		txtId.setBounds(206, 20, 250, 16);
		panel_3.add(txtId);
		
		txtNome = new JTextField();
		txtNome.setText("Nome");
		txtNome.setBounds(206, 48, 250, 28);
		panel_3.add(txtNome);
		txtNome.setColumns(10);
		
		pwdSenha = new JPasswordField();
		pwdSenha.setText("Senha");
		pwdSenha.setBounds(206, 88, 250, 28);
		panel_3.add(pwdSenha);
		
		pwdConfirmaSenha = new JPasswordField();
		pwdConfirmaSenha.setText("Confirma senha");
		pwdConfirmaSenha.setBounds(206, 130, 250, 28);
		panel_3.add(pwdConfirmaSenha);
		
		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Administrador", "Padrão"}));
		comboBox.setBounds(206, 170, 250, 26);
		panel_3.add(comboBox);
		
		tabbedPane.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				menuSelecionado();
			}
		});
	}
	
	private void salvar() {
		if (txtNome.getText().isEmpty()){
			JOptionPane.showMessageDialog(null, "Digite o nome");
			txtNome.requestFocus();
			return;
		}
		String senha = new String(pwdSenha.getPassword());
		String confirmaSenha = new String(pwdConfirmaSenha.getPassword());
		if (senha.isEmpty()){
			JOptionPane.showMessageDialog(null, "Digite a senha");
			pwdSenha.requestFocus();
			return;
		}
		if (confirmaSenha.isEmpty()){
			JOptionPane.showMessageDialog(null, "Digite a confirmação de senha");
			pwdConfirmaSenha.requestFocus();
			return;
		}
		if (!senha.equals(confirmaSenha)) {
			JOptionPane.showMessageDialog(null, "As senhas não coincidem\n\nTente novamente");
			pwdSenha.setText(null);
			pwdConfirmaSenha.setText(null);
			pwdSenha.requestFocus();
			return;
		}
		Usuario usuario = new Usuario();
		try {
			usuario.setIdUsuario(Integer.valueOf(txtId.getText().trim()));
		} catch (Exception e) {
			usuario.setIdUsuario(0);
		}
		usuario.setNome(txtNome.getText().trim());
		usuario.setSenha(senha);
		usuario.setTipo(comboBox.getSelectedItem().toString());
		UsuarioControle.salvar(usuario);
		atualizar();
	}

	private void atualizar() {
		limpar();
		tabbedPane.setSelectedIndex(0);
		atualizarTabela();
		txtPesquisar.requestFocus();
	}

	private void limpar() {
		try {
			String temp = null;
			txtPesquisar.setText(temp);
			txtId.setText(temp);
			txtNome.setText(temp);
			pwdSenha.setText(temp);
			pwdConfirmaSenha.setText(temp);
			comboBox.setSelectedIndex(0);
		} catch (Exception e) {
			// inexistente
		}
	}
	
	private void atualizarTabela() {
		DefaultTableModel model = UsuarioControle.model(txtPesquisar.getText().trim());
		table.setModel(model);
		RowSorter<TableModel> sorter = new TableRowSorter<TableModel>(model);
		table.setRowSorter(sorter);
		//table.getColumnModel().getColumn(0).setPreferredWidth(7);
	}
	
	private void esc(KeyEvent e){
		if (e.getKeyCode() == 27) {// tecla esc
			atualizar();
		}
	}

	private void menuSelecionado() {
		if (tabbedPane.getSelectedIndex() == 0) { // menu tabela
			limpar();
			atualizarTabela();
			txtPesquisar.requestFocus();
		} else if (tabbedPane.getSelectedIndex() == 1) { // menu formul�rio
			novo();
		}
	}
	
	private void novo() {
		limpar();
		txtNome.requestFocus();
	}

	private void cancelar() {
		tabbedPane.setSelectedIndex(0);
	}
	
	private void editar() {
		novo();
		tabbedPane.setSelectedIndex(1);
		int id = Integer.valueOf(table.getValueAt(table.getSelectedRow(), 0).toString());  
		Usuario usuario = UsuarioControle.buscarPorId(id);
		txtId.setText(String.valueOf(usuario.getIdUsuario()));
		txtNome.setText(usuario.getNome());
		pwdSenha.setText(usuario.getSenha());
		pwdConfirmaSenha.setText(usuario.getSenha());
		comboBox.setSelectedItem(usuario.getTipo());
	}
	
	private void deletar() {
		if (txtId.getText().isEmpty()){
			JOptionPane.showMessageDialog(null, "Escolha um usuário clicando duplo na tabela");
			cancelar();
		} else if (JOptionPane.showConfirmDialog(null, "Tem certeza que deseja deletar este usuário?", "Sistema", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			Usuario usuario = new Usuario();
			try {
				usuario.setIdUsuario(Integer.valueOf(txtId.getText().trim()));
			} catch (Exception e) {
				usuario.setIdUsuario(0);
			}
			UsuarioControle.deletar(usuario);
			atualizar();
		}	
	}
		
}
