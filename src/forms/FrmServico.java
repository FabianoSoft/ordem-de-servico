package forms;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.text.MaskFormatter;

import controles.ContatoControle;
import controles.ServicoControle;
import entidades.Contato;
import entidades.Servico;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.RowSorter;
import javax.swing.JFormattedTextField;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import java.awt.CardLayout;
import javax.swing.border.EtchedBorder;
import javax.swing.JButton;
import java.awt.GridLayout;
import java.awt.Font;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Date;
import java.util.Formatter;
import java.util.logging.SimpleFormatter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class FrmServico extends JDialog {

	private JPanel contentPane;
	private JTextField txtDescricao;
	private JTextField txtPesquisar;
	private JTable table;
	private JFormattedTextField frmtdtxtfldDataInicio;
	private JFormattedTextField frmtdtxtfldDataFim;
	private JComboBox comboBoxStatus;
	private JTextField txtValor;
	private JTextArea txtrObs;
	private JComboBox comboBoxTecnico;
	private JComboBox comboBoxCliente;
	private JPanel panelDados;
	private JPanel panelTabela;
	private JTextField txtId;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmServico frame = new FrmServico();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmServico() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent arg0) {
				atualizar();
			}
		});
		setModal(true);
		setResizable(false);
		setTitle("Serviço");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(700, 500);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new CardLayout(0, 0));
		
		panelTabela = new JPanel();
		panelTabela.setBorder(new TitledBorder(null, "Todos os serviços", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		contentPane.add(panelTabela, "name_33157639772478");
		panelTabela.setLayout(null);
		
		JLabel label = new JLabel("Pesquisar:");
		label.setFont(new Font("SansSerif", Font.ITALIC, 12));
		label.setBounds(25, 37, 58, 16);
		panelTabela.add(label);
		
		txtPesquisar = new JTextField();
		txtPesquisar.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				atualizarTabela();
			}
			@Override
			public void keyPressed(KeyEvent e) {
				esc(e);
			}
		});
		txtPesquisar.setText("Pesquisar");
		txtPesquisar.setColumns(10);
		txtPesquisar.setBounds(95, 31, 464, 28);
		panelTabela.add(txtPesquisar);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(25, 71, 636, 365);
		panelTabela.add(scrollPane_1);
		
		table = new JTable();
		table.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == 10) {//10 � a tecla enter
					editar();
				}
			}
		});
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (arg0.getClickCount() > 1) {					
					editar();
				}
			}
		});
		scrollPane_1.setViewportView(table);
		
		JButton btnNovo = new JButton("Novo");
		btnNovo.setMnemonic('n');
		btnNovo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				novo();
			}
		});
		btnNovo.setFont(new Font("SansSerif", Font.BOLD, 12));
		btnNovo.setBounds(571, 31, 90, 28);
		panelTabela.add(btnNovo);
		
		panelDados = new JPanel();
		panelDados.setBorder(new TitledBorder(null, "Dados", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		contentPane.add(panelDados, "name_32898786775939");
		panelDados.setLayout(null);
		
		JLabel lblId = new JLabel("id:");
		lblId.setBounds(175, 35, 13, 16);
		panelDados.add(lblId);
		
		txtDescricao = new JTextField();
		txtDescricao.setText("Descrição");
		txtDescricao.setBounds(200, 69, 319, 28);
		panelDados.add(txtDescricao);
		txtDescricao.setColumns(10);
		
		try {
			MaskFormatter mascaraInicio = new MaskFormatter("##/##/#### ##:##:##");
			frmtdtxtfldDataInicio = new JFormattedTextField(mascaraInicio);
		} catch (Exception e) {
			e.getMessage();
		}
		frmtdtxtfldDataInicio.setText("00000000000000");
		frmtdtxtfldDataInicio.setBounds(200, 109, 135, 28);
		panelDados.add(frmtdtxtfldDataInicio);
		
		try {
			MaskFormatter mascaraFim = new MaskFormatter("##/##/#### ##:##:##");
			frmtdtxtfldDataFim = new JFormattedTextField(mascaraFim);
		} catch (Exception e) {
			e.getMessage();
		}
		frmtdtxtfldDataFim.setText("00000000000000");
		frmtdtxtfldDataFim.setBounds(383, 109, 136, 28);
		panelDados.add(frmtdtxtfldDataFim);
		
		JLabel lblIncio = new JLabel("Início:");
		lblIncio.setBounds(156, 115, 32, 16);
		panelDados.add(lblIncio);
		
		JLabel lblFim = new JLabel("Fim:");
		lblFim.setBounds(347, 115, 24, 16);
		panelDados.add(lblFim);
		
		comboBoxStatus = new JComboBox();
		comboBoxStatus.setModel(new DefaultComboBoxModel(new String[] {"Pendente", "Em execução", "Entregar", "Finalizado"}));
		comboBoxStatus.setBounds(201, 149, 122, 26);
		panelDados.add(comboBoxStatus);
		
		JLabel lblStatus = new JLabel("Status:");
		lblStatus.setBounds(150, 154, 38, 16);
		panelDados.add(lblStatus);
		
		JLabel lblValor = new JLabel("Valor R$:");
		lblValor.setBounds(335, 154, 50, 16);
		panelDados.add(lblValor);
		
		txtValor = new JTextField();
		txtValor.setText("Valor");
		txtValor.setBounds(397, 148, 122, 28);
		panelDados.add(txtValor);
		txtValor.setColumns(10);
		
		JLabel lblObs = new JLabel("Obs.:");
		lblObs.setBounds(159, 272, 29, 16);
		panelDados.add(lblObs);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(200, 263, 319, 74);
		panelDados.add(scrollPane);
		
		txtrObs = new JTextArea();
		scrollPane.setViewportView(txtrObs);
		txtrObs.setText("Obs");
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Controles", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(18, 349, 649, 96);
		panelDados.add(panel_1);
		panel_1.setLayout(new GridLayout(0, 3, 0, 0));
		
		JButton btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				salvar();
			}
		});
		btnSalvar.setFont(new Font("Calibri", Font.BOLD, 18));
		btnSalvar.setMnemonic('s');
		panel_1.add(btnSalvar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelar();
			}
		});
		btnCancelar.setFont(new Font("Calibri", Font.BOLD, 18));
		btnCancelar.setMnemonic('c');
		panel_1.add(btnCancelar);
		
		JButton btnDeletar = new JButton("Deletar");
		btnDeletar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				deletar();
			}
		});
		btnDeletar.setFont(new Font("Calibri", Font.BOLD, 18));
		btnDeletar.setMnemonic('d');
		panel_1.add(btnDeletar);
		
		JLabel lblTcnico = new JLabel("Técnico:");
		lblTcnico.setBounds(142, 192, 46, 16);
		panelDados.add(lblTcnico);
		
		JLabel lblCliente = new JLabel("Cliente:");
		lblCliente.setBounds(146, 230, 42, 16);
		panelDados.add(lblCliente);
		
		comboBoxTecnico = new JComboBox();
		comboBoxTecnico.setBounds(200, 187, 249, 26);
		panelDados.add(comboBoxTecnico);
		
		comboBoxCliente = new JComboBox();
		comboBoxCliente.setBounds(200, 225, 249, 26);
		panelDados.add(comboBoxCliente);
		
		JLabel lblDescrio = new JLabel("Descrição:");
		lblDescrio.setBounds(129, 75, 59, 16);
		panelDados.add(lblDescrio);
		
		txtId = new JTextField();
		txtId.setEditable(false);
		txtId.setText("id");
		txtId.setBounds(201, 29, 122, 28);
		panelDados.add(txtId);
		txtId.setColumns(10);
		
		JButton button = new JButton("...");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				gerenciarContatos();
			}
		});
		button.setFont(new Font("Calibri", Font.BOLD, 37));
		button.setBounds(461, 188, 58, 63);
		panelDados.add(button);
	}

	private void atualizar() {
		preencherComboBoxs();
		limpar();
		atualizarTabela();
	}
	
	private void limpar() {
		String limpar = null;
		txtPesquisar.setText(limpar);
		txtId.setText(limpar);
		txtDescricao.setText(limpar);
		String data = ServicoControle.dateString(new Date());
		frmtdtxtfldDataInicio.setText(data);
		frmtdtxtfldDataFim.setText(data);
		comboBoxStatus.setSelectedIndex(0);
		txtValor.setText(limpar);
		txtrObs.setText(limpar);
		try {
			comboBoxTecnico.setSelectedIndex(0);
			comboBoxCliente.setSelectedIndex(0);			
		} catch (Exception e) {
			// sem dados
		}
	}
	
	private void irParaDados() {
		limpar();
		panelTabela.setVisible(false);
		panelDados.setVisible(true);
		txtDescricao.requestFocus();
	}
	
	private void irParaTabela() {
		limpar();
		atualizarTabela();
		panelDados.setVisible(false);
		panelTabela.setVisible(true);
		txtPesquisar.requestFocus();
	}
	
	private void atualizarTabela() {
		DefaultTableModel model = ServicoControle.model(txtPesquisar.getText().trim());
		table.setModel(model);
		RowSorter<TableModel> sorter = new TableRowSorter<TableModel>(model);
		table.setRowSorter(sorter);
		table.getColumnModel().getColumn(0).setPreferredWidth(50);
		for (int i = 1; i < table.getColumnCount(); i++) {
			table.getColumnModel().getColumn(i).setPreferredWidth(150);
		}
		table.setAutoResizeMode(table.AUTO_RESIZE_OFF);
	}
	
	private void novo() {
		irParaDados();
	}
	
	private void cancelar() {
		irParaTabela();
	}
	
	private void preencherComboBoxs() {
		comboBoxTecnico.removeAllItems();
		comboBoxCliente.removeAllItems();
		for (Contato contato : ContatoControle.listaContatos()) {
			comboBoxTecnico.addItem(ContatoControle.contatoString(contato));
			comboBoxCliente.addItem(ContatoControle.contatoString(contato));
		}
	}
	
	private void salvar() {
		if (txtDescricao.getText().isEmpty()){
			JOptionPane.showMessageDialog(null, "Digite a descrição");
			txtDescricao.requestFocus();
			return;
		}
		Servico servico = new Servico();
		try {
			servico.setIdServico(Integer.valueOf(txtId.getText().trim()));
		} catch (Exception e) {
			servico.setIdServico(0);
		}
		servico.setDescricao(txtDescricao.getText().trim());
		try {
			servico.setDataInicio(ServicoControle.stringDate(frmtdtxtfldDataInicio.getText()));
			servico.setDataFim(ServicoControle.stringDate(frmtdtxtfldDataFim.getText()));			
		} catch (Exception e) {
			// sem data
		}
		servico.setStatus(comboBoxStatus.getSelectedItem().toString());
		try {
			if (txtValor.getText().trim().isEmpty()) {
				txtValor.setText("0,00");
			}
			servico.setValor(Float.valueOf(txtValor.getText().trim().replace(",", ".")));
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Digite um valor numérico");
			txtValor.requestFocus();
			return;
		}
		servico.setObs(txtrObs.getText().trim());
		servico.setTecnico(ContatoControle.stringContato(comboBoxTecnico.getSelectedItem().toString()));
		servico.setCliente(ContatoControle.stringContato(comboBoxCliente.getSelectedItem().toString()));
		ServicoControle.salvar(servico, this);
		irParaTabela();
	}
	
	private void gerenciarContatos() {
		FrmContato contato = new FrmContato();
		contato.setVisible(true);
		preencherComboBoxs();
	}
	
	private void editar() {
		irParaDados();
		int id = Integer.valueOf(table.getValueAt(table.getSelectedRow(), 0).toString());  
		Servico servico = ServicoControle.buscarPorId(id);
		txtId.setText(String.valueOf(servico.getIdServico()));
		txtDescricao.setText(servico.getDescricao());
		if (servico.getDataInicio() != null) {			
			frmtdtxtfldDataInicio.setText(ServicoControle.dateString(servico.getDataInicio()));
		} else {
			frmtdtxtfldDataInicio.setText(null);
		}
		if (servico.getDataFim() != null) {
			frmtdtxtfldDataFim.setText(ServicoControle.dateString(servico.getDataFim()));
		} else {
			frmtdtxtfldDataFim.setText(null);
		}
		comboBoxStatus.setSelectedItem(servico.getStatus());
		txtValor.setText(String.valueOf(servico.getValor()).replace(".", ","));
		txtrObs.setText(servico.getObs());
		comboBoxTecnico.setSelectedItem(ContatoControle.contatoString(servico.getTecnico()));
		comboBoxCliente.setSelectedItem(ContatoControle.contatoString(servico.getCliente()));
	}
	
	private void deletar() {
		if (txtId.getText().isEmpty()){
			JOptionPane.showMessageDialog(null, "Escolha uma Ordem de Serviço clicando duplo na tabela");
			irParaTabela();
		} else if (JOptionPane.showConfirmDialog(null, "Tem certeza que deseja deletar esta ordem de serviço?", "Sistema", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			Servico servico = new Servico();
			try {
				servico.setIdServico(Integer.valueOf(txtId.getText().trim()));
			} catch (Exception e) {
				servico.setIdServico(0);
			}
			ServicoControle.deletar(servico);
			irParaTabela();;
		}	
	}
	
	private void esc(KeyEvent e){
		if (e.getKeyCode() == 27) {// tecla esc
			atualizar();
		}
	}
}
